package ChatSG;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Requests {

	public String getChat() {
		try {
			URL url = new URL("https://relevant.cynnfx.fr/api/chats/1");
			HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
			httpURLConnection.setRequestMethod("GET");

			String line = "";
			InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			StringBuilder response = new StringBuilder();
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
			}
			bufferedReader.close();
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void sendPostMessage(String content) {
		try {
			String data = "user_id=33"+ "&content=" + content;

			URL url = new URL("https://relevant.cynnfx.fr/api/messages");
			HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
			httpURLConnection.setRequestMethod("POST");

			httpURLConnection.setDoOutput(true);
			
			OutputStream outputStream = httpURLConnection.getOutputStream();
			outputStream.write(data.getBytes());
			outputStream.flush();
			outputStream.close();
			
			System.out.println(httpURLConnection.getResponseCode());

			String line = "";
			InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			StringBuilder response = new StringBuilder();
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
			}
			bufferedReader.close();
			attachMessage(response.toString());
			System.out.println("Message sent succesfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void attachMessage(String responseMessage) {
	    JsonObject responseJsonObj = JsonParser.parseString(responseMessage.toString()).getAsJsonObject();
        JsonElement messageID = responseJsonObj.get("id");

		try {
			URL url = new URL("https://relevant.cynnfx.fr/api/chats/1/messageAttach/" + messageID);
			HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
			httpURLConnection.setRequestMethod("GET");

			String line = "";
			InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			StringBuilder response = new StringBuilder();
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
			}
			bufferedReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
