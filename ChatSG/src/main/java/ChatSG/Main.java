package ChatSG;

import java.util.ArrayList;
import com.google.gson.*;

public class Main {

	static ChatSG.Requests obj = new ChatSG.Requests();
	static ArrayList<String> msgs = new ArrayList<String>();
	static ArrayList<String> usrs = new ArrayList<String>();
	static ArrayList<String> dts = new ArrayList<String>();

	public static void main(String[] args) {
		setData();
		if (args.length == 1 && args[0].equals("displayChat")) {
			displayChat();
		}
		else if (args.length == 2 && args[0].equals("postMessage")) {
			obj.sendPostMessage(args[1]);
		}
		else if (args.length == 1 && args[0].equals("displayLast5")) {
			displayLast5();
		}
		else {
			System.out.println("Please enter correct arguments as instructed below");
			System.out.println("displayChat: Displays all the messages of the anonymous chat");
			System.out.println("displayLast5: Displays last 5 messages of the anonymous chat");
			System.out.println("postMessage {your username or id} {your message}: Sends an anonymous message to the chat");
		}
	}

	private static void setData() {
		String response = obj.getChat();
	    JsonObject jsonObj = JsonParser.parseString(response.toString()).getAsJsonObject();
        JsonElement messagesElem = jsonObj.get("messages");
        JsonArray messagesJsonArray = messagesElem.getAsJsonArray();

        for (int i = 0; i < messagesJsonArray.size(); i++) {
            JsonObject tmp = messagesJsonArray.get(i).getAsJsonObject();
            msgs.add(tmp.get("content").toString());
            usrs.add(tmp.get("user_id").toString());
            dts.add(tmp.get("created_at").toString());
        }
	}

	private static void displayChat() {
		for (int i = 0; i < msgs.size(); i++) {
			System.out.println("user" + usrs.get(i) + " --- " + dts.get(i));
			System.out.println(msgs.get(i));
			System.out.println("");
		}
	}

	private static void displayLast5() {
		for (int i = 0; i < msgs.size(); i++) {
			if (msgs.size() >= 5 && i >= (msgs.size() - 5)) {
				System.out.println("user" + usrs.get(i) + " --- " + dts.get(i));
				System.out.println(msgs.get(i));
				System.out.println("");
			}
			else {
				displayChat();
			}
		}
	}
}
